import { ReactChild } from "react"

type LayoutProps = {
  attributes?: object,
  title?: string,
  subtitle?: string,
  children: ReactChild | ReactChild[]
}

type NavItem = {
  title: string,
  route: string,
  children?: NavItem[]
}


type NavProps = {
  attributes: object
}