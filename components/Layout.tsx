import { LayoutProps } from '../onboardingProject'

import Navigation from './Navigation'
const Layout = (props: LayoutProps) => (
  <div className="flex flex-wrap w-1/2 mx-auto">
    <Navigation 
      attributes={{
        id: 'main-nav',
      }}
    />
    <main {...props.attributes}>
      <div>
        <h1>
          <span>{props.title}</span>
          {
            props.subtitle && (
              <span> - {props.subtitle}</span>
            )
          }
        </h1>
      </div>
      <div>
        { props.children }
      </div>
    </main>  
  </div>
)

export default Layout