import { NavProps, NavItem } from '../onboardingProject'

import Link from 'next/link'

import { navItems } from '../lib/navItems'

const navClasses = [
  'px-10',
  'list-none',
]

const Navigation = (props: NavProps) => {
  return (
    <nav {...props.attributes}>
      <ul className={navClasses.join(' ')}>
        {
          navItems.map((item: NavItem, index: number) => {
            return <li key={index}>
              <Link href={item.route}>
                <a>
                  { item.title }
                </a>
              </Link>
              {
                item.children !== undefined && (
                  <ul className={navClasses.join(' ')}>
                    {
                      item.children.map((child: NavItem, index: number) => {
                        return <li key={index}>
                          <Link href={child.route}>
                            <a>{child.title}</a>
                          </Link>
                        </li>
                      })
                    }
                  </ul>
                )
              }
            </li>
          })
        }
      </ul>
    </nav>
  )
}

export default Navigation