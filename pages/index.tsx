import Head from 'next/head'
import Link from 'next/link'

export default function Home() {
  return (
    <div className="flex flex-col justify-center min-h-screen py-2 sm:items-center">
      <Head>
        <title>UConn Communications Onboarding</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className="flex flex-col justify-center max-w-6xl flex-1 px-4 my-20">
        <h1 className="text-4xl font-bold sm:text-6xl">
          Welcome to the UComm Web Team!
        </h1>

        <div className='my-10'>
          <p className="text-xl sm:text-2xl">
            These onboarding docs are meant to be a practical guide to help you get started working with us. While they're not exhaustive, they should give you a good start and help to answer some of your initial
          </p>
        </div>

        <div className="flex flex-wrap items-center justify-around max-w-4xl mt-6 sm:w-full">
          <Link href="/people">
            <a
              className="p-6 mt-6 text-left border w-96 rounded-xl hover:text-blue-600 focus:text-blue-600"
            >
              <h2 className="text-2xl font-bold">People &rarr;</h2>
              <p className="mt-4 text-xl">
                An overview of the people you'll work with.
              </p>
            </a>
          </Link>

          <Link href="/principles">
            <a
              className="p-6 mt-6 text-left border w-96 rounded-xl hover:text-blue-600 focus:text-blue-600"
            >
              <h2 className="text-2xl font-bold">Principles &rarr;</h2>
              <p className="mt-4 text-xl">
                Guiding principles for our department.
              </p>
            </a>
          </Link>
          <Link href="/technology">
            <a
              className="p-6 mt-6 text-left border w-96 rounded-xl hover:text-blue-600 focus:text-blue-600"
            >
              <h2 className="text-2xl font-bold">Tech/Servers &rarr;</h2>
              <p className="mt-4 text-xl">
                Our tech stack and the servers projects run on.
              </p>
            </a>          
          </Link>
          <Link href="/projects">
            <a
              className="p-6 mt-6 text-left border w-96 rounded-xl hover:text-blue-600 focus:text-blue-600"
            >
              <h2 className="text-2xl font-bold">Projects &rarr;</h2>
              <p className="mt-4 text-xl">
                An overview of some of our more important projects.
              </p>
            </a>         
          </Link>
        </div>
      </main>

      <footer className="flex items-center justify-center w-full h-24 border-t">
        <a
          className="flex items-center justify-center"
          href="https://communications.uconn.edu"
          target="_blank"
          rel="noopener noreferrer"
        >
          University of Connecticut Office of University Communications
        </a>
      </footer>
    </div>
  )
}
