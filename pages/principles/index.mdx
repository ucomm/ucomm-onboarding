import Layout from '../../components/Layout'

## Introduction

The web development team operates on a few core principles

- Accessibility
- Collaboration
- Reproducability
- Resiliency

These principles will help you make decisions about how to build and maintain projects

## Accessibility

We've all had the experience of using sites and apps that are just bad. Maybe it's the interface, maybe it's response times, or too much animation. We try to make sure that we adhere to the [WCAG 2.1 standards](https://www.w3.org/TR/WCAG21/) for accessibility. This includes ensuring:

- sufficient color contrast
- keyboard navigation
- semantic HTML with appropriate roles and aria attributes
- screen reader support
- progressive enhancemnent

Often in meetings we need to ensure that we advocate for accessibility best practices. Typically it's best to bring up accessibility issues sooner in the development process rather than later. 

## Collaboration

When in doubt, ask a question because none of us has all the answers. We have to communicate. We have to find out why we're doing things the way we are. In order to be successful on any project, we need to bring together a variety of experiences and perspectives.

It should go without saying that this should hold true within our own office. However, we also need to be responsive to people in a variety of departments at all levels. These include representatives from ITS, Health, Schools and Colleges, the President's and Provost's Offices, and our regional campuses. There are times when we need to dig to understand why people are making their requests. Our job isn't to rubber stamp decisions. It's to work with people to reach the _best_ decisions. That means saying things like "What would it look like if..." or "I see some potential issues here and..." or even (very nicely) "No" are important.

## Reproducability

Projects need to be easily shared within the team. The reproducability principle informs decisions about how projects get organized from the smallest detail up to production deployment. At the project level, things like lock files, resource control files, and configuration are all critical. The README for each project should explain either what each one does or how to use it. Take for example `.nvmrc` files. In a project with one of these, running `nvm use` will guarantee that the same version of nodeJS regardless of platform. This includes during build processes on Jenkins or Bitbucket Pipelines.

Going up a level, it's the same reason we use [docker](https://docker.com) for local development. By using a containerized system, we can make sure that our projects' configuration runs the same way in any environment. Project development slows down when each person uses a different toolset.

Writing clear documentation and doing things like typing functions and methods is also important. In a large project, there's a massive difference between these two examples.

```php
<?php

function getUser($id) {
  return user($id);
}
```

```php

<?php
/*
* Get user data including: ...
*
* @param $id int the ID of a user
* @return array data about the requested user
*/
function getUser(int $id): array {
  return user($id);
}
```

Fine... That example may be a little silly... But the point is still valid. For someone new to a project, it's important that they know what to expect from it.

## Resiliency

If for some reason, the entire web dev team disappeared, would the next group be able to pick up where we left off? At any time, each person in the dev team needs to be able to take over with as little difficulty as possible. Sometimes people are sick, on vacation, or the unexpected happens. By trying to consolidate tools and approaches each project can be similar. While there will be variations of course, having a common base to work from means less confusion when trying to share projects. 

Resiliency also means that when projects go into production: errors are handled in useful ways, the project can fail safely, and things still work in less than ideal environments. For instance, in a javascript heavy project, will it crash if there's an error? Is it still useable if the JS doesn't load?

export default ({ children }) => ( 
  <Layout
    title="Principles"
  >
    { children }
  </Layout>
)