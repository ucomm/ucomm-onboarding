import '../styles/globals.css'
import '../styles/banner.css'

import type { AppProps } from 'next/app'
import Banner from '../components/Banner'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
    <Banner />
    <div className='font-sans'>
      <Component {...pageProps} />
    </div>
    </>
  )
}

export default MyApp
