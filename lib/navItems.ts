import { NavItem } from "../onboardingProject"

export const navItems: Array<NavItem> = [
  {
    title: 'Home',
    route: '/'
  },
  {
    title: 'People',
    route: '/people'
  },
  {
    title: 'Principles',
    route: '/principles'
  },
  {
    title: 'Tech/Servers',
    route: '/technology'
  },
  {
    title: 'Projects',
    route: '/projects',
    children: [
      {
        title: 'Communication',
        route: '/projects/communication'
      },
      {
        title: 'Workflow',
        route: '/projects/communication'
      }
    ]
  },
]